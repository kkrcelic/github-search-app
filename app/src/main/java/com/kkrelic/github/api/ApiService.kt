package com.kkrelic.github.api

import com.google.gson.GsonBuilder
import com.kkrcelic.github.BuildConfig
import com.kkrelic.github.data.Owner
import com.kkrelic.github.data.Repository
import io.reactivex.Observable
import io.reactivex.Single
import java.util.TimeZone
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("/search/repositories")
    fun getRepositories(
        @Query("q") searchTerm: String,
        @Query("sort") sortType: String?,
        @Query("per_page") perPage: Int,
        @Query("page") pageNumber: Int
    ): Single<RepositoriesApiResponse>

    @GET("/users/{user}")
    fun getOwnerDetails(@Path("user") ownerName: String): Observable<Owner>

    @GET("/repos/{ownerLiveData}/{repositoryLiveData}")
    fun getRepositoryDetails(
        @Path("ownerLiveData") ownerLogin: String,
        @Path("repositoryLiveData") repositoryName: String
    ): Observable<Repository>

    companion object {
        fun create(): ApiService {
            val okHttpBuilder = OkHttpClient.Builder()

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.HEADERS else HttpLoggingInterceptor.Level.BASIC
            okHttpBuilder.addInterceptor(loggingInterceptor)

            okHttpBuilder.addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder()
                    .addHeader("User-Agent", "GithubSearch")
                    .addHeader("Accept", "application/vnd.github.v3+json")
                    .addHeader("Time-Zone", TimeZone.getDefault().id)
                    .build())
            }

            val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpBuilder.build())
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}
