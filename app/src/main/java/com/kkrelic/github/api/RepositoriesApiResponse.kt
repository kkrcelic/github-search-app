package com.kkrelic.github.api

import com.google.gson.annotations.SerializedName
import com.kkrelic.github.data.Repository

data class RepositoriesApiResponse(
    @SerializedName("items")
    var items: List<Repository>
)
