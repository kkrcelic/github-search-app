package com.kkrelic.github.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kkrelic.github.repository.RepositoriesRepository
import com.kkrelic.github.ui.repositories.RepositoriesViewModel
import com.kkrelic.github.ui.repository.RepositoryDetailsViewModel

class RepositoriesViewModelFactory(private val repository: RepositoriesRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RepositoriesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RepositoriesViewModel(repository) as T
        } else if (modelClass.isAssignableFrom(RepositoryDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RepositoryDetailsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
