package com.kkrelic.github.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kkrelic.github.repository.OwnerRepository
import com.kkrelic.github.ui.owner.OwnerDetailsViewModel

class OwnerViewModelFactory(private val repository: OwnerRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OwnerDetailsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return OwnerDetailsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
