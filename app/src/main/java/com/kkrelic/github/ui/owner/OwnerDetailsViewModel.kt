package com.kkrelic.github.ui.owner

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kkrelic.github.data.Owner
import com.kkrelic.github.data.Resource
import com.kkrelic.github.repository.OwnerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class OwnerDetailsViewModel(private val repository: OwnerRepository) : ViewModel() {

    val ownerLiveData = MutableLiveData<Resource<Owner>>()
    private var disposable: Disposable? = null

    fun getOwnerDetails(ownerName: String) {
        ownerLiveData.value = Resource.loading(null)
        disposable = repository.getOwnerDetails(ownerName).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { owner ->
                    ownerLiveData.value = Resource.success(owner)
                },
                { error ->
                    ownerLiveData.value =
                        Resource.error(error.localizedMessage, null)
                }
            )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}
