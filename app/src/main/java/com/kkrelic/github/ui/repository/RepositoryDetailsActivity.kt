package com.kkrelic.github.ui.repository

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kkrcelic.github.R
import com.kkrelic.github.api.ApiService
import com.kkrelic.github.data.Repository
import com.kkrelic.github.data.ResourceStatus
import com.kkrelic.github.repository.RepositoriesRepository
import com.kkrelic.github.ui.base.BaseActivity
import com.kkrelic.github.ui.owner.OwnerDetailsActivity
import com.kkrelic.github.util.Constants
import com.kkrelic.github.viewmodel.RepositoriesViewModelFactory
import java.text.SimpleDateFormat
import java.util.Locale
import kotlinx.android.synthetic.main.activity_repository_details.*
import kotlinx.android.synthetic.main.activity_repository_details.openInBrowserButton
import kotlinx.android.synthetic.main.content_view.view.*
import kotlinx.android.synthetic.main.toolbar.*

class RepositoryDetailsActivity : BaseActivity() {

    private lateinit var viewModel: RepositoryDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_details)

        initViewModel()
        setupToolbar()

        val repositoryName = intent.getStringExtra(Constants.EXTRA_REPOSITORY_NAME)
        val ownerLogin = intent.getStringExtra(Constants.EXTRA_OWNER_LOGIN)
        viewModel.getRepositoryDetails(ownerLogin, repositoryName)

        viewModel.repositoryLiveData.observe(this, Observer {
            when (it.status) {
                ResourceStatus.LOADING -> {
                    showProgressSpinner()
                    repositoryDetailsContainer.visibility = View.GONE
                }
                ResourceStatus.SUCCESS -> {
                    it.data?.let { repository ->
                        hideProgressSpinner()
                        showRepositoryDetails(repository)
                    }
                }
                ResourceStatus.ERROR -> {
                    it.message?.let { message ->
                        hideProgressSpinner()
                        showErrorDialog(message)
                    }
                }
            }
        })

        ownerContainer.setOnClickListener {
            viewModel.repositoryLiveData.value?.data?.let { repository ->
                goToOwnerDetails(repository.owner.login)
            }
        }

        openInBrowserButton.setOnClickListener {
            viewModel.repositoryLiveData.value?.data?.let { repository ->
                openInBrowser(repository.url)
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this,
            RepositoriesViewModelFactory(
                RepositoriesRepository(ApiService.create())
            )
        )
            .get(RepositoryDetailsViewModel::class.java)
    }

    private fun showRepositoryDetails(repository: Repository) {

        repositoryDetailsContainer.visibility = View.VISIBLE
        toolbar?.title = repository.name

        if (!repository.description.isNullOrBlank()) {
            descriptionContainer.visibility = View.VISIBLE
            descriptionContainer.contentTextView.text = repository.description
        } else {
            descriptionContainer.visibility = View.GONE
        }

        watchersContainer.contentTextView.text = repository.numWatchers.toString()
        issuesContainer.contentTextView.text = repository.numIssues.toString()
        forksContainer.contentTextView.text = repository.numForks.toString()
        subscribersContainer.contentTextView.text = repository.numSubscribers.toString()

        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy HH:MM", Locale.UK)
        val date = simpleDateFormat.format(repository.updatedAt)
        lastUpdatedAtContainer.contentTextView.text = date.toString()

        if (!repository.programmingLanguage.isNullOrBlank()) {
            languageContainer.visibility = View.VISIBLE
            languageContainer.contentTextView.text = repository.programmingLanguage
        } else {
            languageContainer.visibility = View.GONE
        }

        if (!repository.defaultBranch.isNullOrBlank()) {
            defaultBranchContainer.visibility = View.VISIBLE
            defaultBranchContainer.contentTextView.text = repository.defaultBranch
        } else {
            defaultBranchContainer.visibility = View.GONE
        }

        ownerContainer.contentTextView.text = repository.owner.login
    }

    private fun goToOwnerDetails(ownerLogin: String) {
        var intent = Intent(this, OwnerDetailsActivity::class.java)
        intent.putExtra(Constants.EXTRA_OWNER_LOGIN, ownerLogin)
        startActivity(intent)
    }

    override fun showProgressSpinner() {
        super.showProgressSpinner()
        repositoryDetailsContainer.visibility = View.GONE
    }
}
