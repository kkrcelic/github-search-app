package com.kkrelic.github.ui.repositories

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding2.widget.queryTextChanges
import com.kkrcelic.github.R
import com.kkrelic.github.api.ApiService
import com.kkrelic.github.repository.RepositoriesRepository
import com.kkrelic.github.ui.base.BaseActivity
import com.kkrelic.github.viewmodel.RepositoriesViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import kotlinx.android.synthetic.main.activity_repositories.*

class RepositoriesActivity : BaseActivity() {

    private lateinit var viewModel: RepositoriesViewModel
    private lateinit var repositoriesListAdapter: RepositoriesAdapter

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repositories)

        initViewModel()
        initAdapter()
        initNetworkState()
        initSortDropdown()

        disposable = searchRepositoriesView.queryTextChanges().debounce(500, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread()).subscribe {
                searchRepositories(searchRepositoriesView.query.toString().trim(), sortDropdown.selectedItem as String)
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this,
            RepositoriesViewModelFactory(
                RepositoriesRepository(ApiService.create())
            )
        ).get(RepositoriesViewModel::class.java)
    }

    private fun initAdapter() {

        repositoriesListAdapter =
            RepositoriesAdapter { viewModel.retry() }
        repositoriesView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        repositoriesView.adapter = repositoriesListAdapter

        viewModel.repositories.observe(this, Observer {
            repositoriesListAdapter.submitList(it)
        })
    }

    private fun initNetworkState() {
        viewModel.networkState.observe(this, Observer {
            repositoriesListAdapter.setNetworkState(it)
        })
    }

    private fun initSortDropdown() {

        val dropdownAdapter = ArrayAdapter<String>(this, R.layout.item_dropwdown)
        dropdownAdapter.add("stars")
        dropdownAdapter.add("forks")
        dropdownAdapter.add("updated")
        sortDropdown.adapter = dropdownAdapter

        sortDropdown.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                searchRepositories(searchRepositoriesView.query.toString().trim(), sortDropdown.selectedItem as String)
            }
        }
    }

    private fun searchRepositories(searchTerm: String, sortType: String?) {
        repositoriesListAdapter.submitList(null)
        repositoriesListAdapter.setNetworkState(null)
        repositoriesView.scrollToPosition(0)
        hideKeyboard()
        if (!searchTerm.isBlank()) {
            viewModel.searchRepositories(searchTerm, sortType)
        }
    }

    private fun hideKeyboard() {
        if (searchRepositoriesView.hasFocus()) searchRepositoriesView.clearFocus()
    }
}
