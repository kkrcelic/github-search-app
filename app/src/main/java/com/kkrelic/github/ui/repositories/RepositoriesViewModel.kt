package com.kkrelic.github.ui.repositories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.map
import androidx.lifecycle.Transformations.switchMap
import androidx.lifecycle.ViewModel
import com.kkrelic.github.repository.RepositoriesRepository

class RepositoriesViewModel(private val repository: RepositoriesRepository) : ViewModel() {

    private val searchTerm = MutableLiveData<String>()
    private val sortType = MutableLiveData<String>()

    private val searchResult = switchMap(searchTerm) { searchTerm ->
        map(sortType) { sortType -> repository.searchRepositories(searchTerm, sortType) }
    }

    val repositories = switchMap(searchResult) { it.pagedList }
    val networkState = switchMap(searchResult) {
        it.networkState
    }

    fun searchRepositories(searchTerm: String, sortType: String?) {
        if (this.searchTerm.value == searchTerm && this.sortType.value == sortType) {
            return
        }
        this.searchTerm.value = searchTerm
        this.sortType.value = sortType
    }

    fun retry() {
        val listing = searchResult.value
        listing?.retry?.invoke()
    }
}
