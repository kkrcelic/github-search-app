package com.kkrelic.github.ui.repository

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kkrelic.github.data.Repository
import com.kkrelic.github.data.Resource
import com.kkrelic.github.repository.RepositoriesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RepositoryDetailsViewModel(private val repository: RepositoriesRepository) : ViewModel() {

    val repositoryLiveData = MutableLiveData<Resource<Repository>>()
    var disposable: Disposable? = null

    fun getRepositoryDetails(ownerName: String, repoName: String) {
        repositoryLiveData.value = Resource.loading(null)
        disposable = repository.getRepositoryDetails(ownerName, repoName).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                { repository ->
                    repositoryLiveData.value =
                        Resource.success(repository)
                },
                { error ->
                    repositoryLiveData.value =
                        Resource.error(error.localizedMessage, null)
                }
            )
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}
