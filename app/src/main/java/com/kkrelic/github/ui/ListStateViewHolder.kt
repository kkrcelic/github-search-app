package com.kkrelic.github.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kkrcelic.github.R
import com.kkrelic.github.data.NetworkState
import com.kkrelic.github.data.ResourceStatus
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_state.*

class ListStateViewHolder(override val containerView: View, retryCallback: () -> Unit) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

    init {
        itemView.setOnClickListener {
            retryCallback()
        }
    }

    fun bind(state: NetworkState?) {
        state?.let {
            when (state.status) {
                ResourceStatus.LOADING -> {
                    progressSpinner.visibility = View.VISIBLE
                    stateContainer.visibility = View.GONE
                }
                ResourceStatus.ERROR -> {
                    progressSpinner.visibility = View.GONE
                    stateContainer.visibility = View.VISIBLE
                    errorMessageTextView.text = state.errorMessage
                }
                else -> {
                    progressSpinner.visibility = View.GONE
                    stateContainer.visibility = View.GONE
                }
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): ListStateViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_state, parent, false)
            return ListStateViewHolder(view, retryCallback)
        }
    }
}
