package com.kkrelic.github.ui.owner

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.kkrcelic.github.R
import com.kkrelic.github.GlideApp
import com.kkrelic.github.api.ApiService
import com.kkrelic.github.data.Owner
import com.kkrelic.github.data.ResourceStatus
import com.kkrelic.github.repository.OwnerRepository
import com.kkrelic.github.ui.base.BaseActivity
import com.kkrelic.github.util.Constants
import com.kkrelic.github.viewmodel.OwnerViewModelFactory
import kotlinx.android.synthetic.main.activity_owner_details.*
import kotlinx.android.synthetic.main.content_view.view.*
import kotlinx.android.synthetic.main.toolbar.*

class OwnerDetailsActivity : BaseActivity() {

    private lateinit var viewModel: OwnerDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_owner_details)

        initViewModel()
        setupToolbar()

        val ownerLogin = intent.getStringExtra(Constants.EXTRA_OWNER_LOGIN)
        viewModel.getOwnerDetails(ownerLogin)

        viewModel.ownerLiveData.observe(this, Observer {
            when (it.status) {
                ResourceStatus.LOADING -> {
                    showProgressSpinner()
                    ownerDetailsContainer.visibility = View.GONE
                }
                ResourceStatus.SUCCESS -> {
                    it.data?.let { owner ->
                        hideProgressSpinner()
                        showOwnerDetails(owner)
                    }
                }
                ResourceStatus.ERROR -> {
                    it.message?.let { message ->
                        hideProgressSpinner()
                        showErrorDialog(message)
                    }
                }
            }
        })

        openInBrowserButton.setOnClickListener {
            viewModel.ownerLiveData.value?.data?.let { owner ->
                openInBrowser(owner.url)
            }
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, OwnerViewModelFactory(
            OwnerRepository(ApiService.create())
        )
        ).get(OwnerDetailsViewModel::class.java)
    }

    private fun showOwnerDetails(owner: Owner) {

        ownerDetailsContainer.visibility = View.VISIBLE
        toolbar?.title = owner.login

        GlideApp.with(imageView.context).load(owner.avatarUrl).into(imageView)

        publicReposContainer.contentTextView.text = owner.numPublicRepos.toString()
        followersContainer.contentTextView.text = owner.numFollowers.toString()
        followingContainer.contentTextView.text = owner.numFollowing.toString()

        if (!owner.name.isNullOrBlank()) {
            nameContainer.visibility = View.VISIBLE
            nameContainer.contentTextView.text = owner.name
        } else {
            nameContainer.visibility = View.GONE
        }

        if (!owner.bio.isNullOrBlank()) {
            bioContainer.visibility = View.VISIBLE
            bioContainer.contentTextView.text = owner.bio
        } else {
            bioContainer.visibility = View.GONE
        }

        if (!owner.location.isNullOrBlank()) {
            locationContainer.visibility = View.VISIBLE
            locationContainer.contentTextView.text = owner.location
        } else {
            locationContainer.visibility = View.GONE
        }

        if (!owner.company.isNullOrBlank()) {
            companyContainer.visibility = View.VISIBLE
            companyContainer.contentTextView.text = owner.company
        } else {
            companyContainer.visibility = View.GONE
        }

        if (!owner.blog.isNullOrBlank()) {
            blogContainer.visibility = View.VISIBLE
            blogContainer.contentTextView.text = owner.blog
        } else {
            blogContainer.visibility = View.GONE
        }
    }

    override fun showProgressSpinner() {
        super.showProgressSpinner()
        ownerDetailsContainer.visibility = View.GONE
    }
}
