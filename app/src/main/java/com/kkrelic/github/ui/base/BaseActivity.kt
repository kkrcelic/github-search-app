package com.kkrelic.github.ui.base

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.kkrcelic.github.R
import kotlinx.android.synthetic.main.progress_spinner.*
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity : AppCompatActivity() {

    protected fun showErrorDialog(message: String) {
        AlertDialog.Builder(this).setTitle("Error ocured!")
            .setMessage(message).setCancelable(false)
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

    protected open fun showProgressSpinner() {
        progressSpinner.visibility = View.VISIBLE
    }

    protected fun hideProgressSpinner() {
        progressSpinner.visibility = View.GONE
    }

    protected fun openInBrowser(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    protected fun setupToolbar() {
        toolbar?.setNavigationIcon(R.drawable.icn_arrow_back)
        toolbar?.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}
