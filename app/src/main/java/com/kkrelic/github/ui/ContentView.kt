package com.kkrelic.github.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.kkrcelic.github.R
import kotlinx.android.synthetic.main.content_view.view.*

class ContentView : LinearLayout {

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int?) {
        LayoutInflater.from(getContext()).inflate(R.layout.content_view, this, true)

        if (attrs != null && defStyleAttr != null) {
            val typedArray = context.obtainStyledAttributes(attrs, R.styleable.ContentView, defStyleAttr, 0)
            try {
                if (!typedArray.getText(R.styleable.ContentView_titleText).isNullOrBlank()) {
                    title.text = typedArray.getText(R.styleable.ContentView_titleText)
                }
                if (!typedArray.getText(R.styleable.ContentView_contentText).isNullOrBlank()) {
                    contentTextView.text = typedArray.getText(R.styleable.ContentView_contentText)
                }
            } finally {
                typedArray.recycle()
            }
        }
    }
}
