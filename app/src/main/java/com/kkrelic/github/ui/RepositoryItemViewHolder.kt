package com.kkrelic.github.ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kkrcelic.github.R
import com.kkrelic.github.GlideApp
import com.kkrelic.github.data.Repository
import com.kkrelic.github.ui.owner.OwnerDetailsActivity
import com.kkrelic.github.ui.repository.RepositoryDetailsActivity
import com.kkrelic.github.util.Constants
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_repository.*

class RepositoryItemViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    var repository: Repository? = null

    init {
        itemView.setOnClickListener {
            repository?.let { repository ->
                val intent = Intent(itemView.context, RepositoryDetailsActivity::class.java)
                intent.putExtra(Constants.EXTRA_REPOSITORY_NAME, repository.name)
                intent.putExtra(Constants.EXTRA_OWNER_LOGIN, repository.owner.login)
                itemView.context.startActivity(intent)
            }
        }

        ownerImageView.setOnClickListener {
            repository?.owner?.let { owner ->
                val intent = Intent(itemView.context, OwnerDetailsActivity::class.java)
                intent.putExtra(Constants.EXTRA_OWNER_LOGIN, owner.login)
                itemView.context.startActivity(intent)
            }
        }
    }

    fun bind(repository: Repository?) {
        this.repository = repository
        repository?.let {
            GlideApp.with(ownerImageView.context).load(repository.owner.avatarUrl).into(ownerImageView)
            repositoryNameTextView.text = repository.fullName
            ownerLoginTextView.text = repository.owner.login
            issuesTextView.text = repository.numIssues.toString()
            forksTextView.text = repository.numForks.toString()
            watchersTextView.text = repository.numWatchers.toString()
        }
    }

    companion object {
        fun create(parent: ViewGroup): RepositoryItemViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_repository, parent, false)
            return RepositoryItemViewHolder(view)
        }
    }
}
