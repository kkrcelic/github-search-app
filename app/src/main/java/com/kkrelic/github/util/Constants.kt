package com.kkrelic.github.util

object Constants {
    const val EXTRA_OWNER_LOGIN = "extra.OWNER_LOGIN"
    const val EXTRA_REPOSITORY_NAME = "extra.REPOSITORY_NAME"
}
