package com.kkrelic.github.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.kkrelic.github.api.ApiService
import io.reactivex.disposables.CompositeDisposable

class RepositoriesDataSourceFactory(
    private val searchQuery: String,
    private val sortType: String?,
    private val apiService: ApiService,
    private val compositeDisposable: CompositeDisposable
) :
        DataSource.Factory<Int, Repository>() {

    val repositoriesDataSourceLiveData = MutableLiveData<RepositoriesDataSource>()

    override fun create(): DataSource<Int, Repository> {
        val repositoriesDataSource = RepositoriesDataSource(
            searchQuery,
            sortType,
            apiService,
            compositeDisposable
        )
        repositoriesDataSourceLiveData.postValue(repositoriesDataSource)
        return repositoriesDataSource
    }
}
