package com.kkrelic.github.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Owner(
    @SerializedName("html_url")
    var url: String,
    @SerializedName("avatar_url")
    var avatarUrl: String,
    @SerializedName("login")
    var login: String,
    @SerializedName("name")
    var name: String?,
    @SerializedName("bio")
    var bio: String?,
    @SerializedName("location")
    var location: String?,
    @SerializedName("company")
    var company: String?,
    @SerializedName("blog")
    var blog: String?,
    @SerializedName("public_repos")
    var numPublicRepos: Int,
    @SerializedName("followers")
    var numFollowers: Int,
    @SerializedName("following")
    var numFollowing: Int
) : Parcelable
