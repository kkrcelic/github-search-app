package com.kkrelic.github.data

enum class ResourceStatus {
    LOADING,
    SUCCESS,
    ERROR
}
