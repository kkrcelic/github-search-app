package com.kkrelic.github.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.Date
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Repository(
    @SerializedName("name")
    var name: String,
    @SerializedName("full_name")
    var fullName: String,
    @SerializedName("owner")
    var owner: Owner,
    @SerializedName("watchers_count")
    var numWatchers: Int,
    @SerializedName("forks_count")
    var numForks: Int,
    @SerializedName("open_issues_count")
    var numIssues: Int,
    @SerializedName("subscribers_count")
    var numSubscribers: Int,
    @SerializedName("html_url")
    var url: String,
    @SerializedName("created_at")
    var createdAt: Date,
    @SerializedName("updated_at")
    var updatedAt: Date,
    @SerializedName("language")
    var programmingLanguage: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("default_branch")
    var defaultBranch: String?
) : Parcelable
