package com.kkrelic.github.data

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.kkrelic.github.api.ApiService
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class RepositoriesDataSource(
    private val searchQuery: String,
    private val sortType: String?,
    private val apiService: ApiService,
    private val compositeDisposable: CompositeDisposable
) :
        PageKeyedDataSource<Int, Repository>() {

    var networkState: MutableLiveData<NetworkState> = MutableLiveData()
    private var retryCompletable: Completable? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Repository>
    ) {
        updateState(NetworkState.LOADING)
        compositeDisposable.add(apiService.getRepositories(searchQuery, sortType, params.requestedLoadSize, 1)
                .subscribe(
                    { response ->
                        updateState(NetworkState.SUCCESS)
                        callback.onResult(response.items, null, 2)
                    },
                    { error ->
                        updateState(NetworkState.error(error.localizedMessage))
                        setRetry(Action { loadInitial(params, callback) })
                    }
                )
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
        updateState(NetworkState.LOADING)
        compositeDisposable.add(apiService.getRepositories(searchQuery, sortType, params.requestedLoadSize, params.key)
                .subscribe(
                    { response ->
                        updateState(NetworkState.SUCCESS)
                        callback.onResult(response.items, params.key + 1)
                    },
                    { error ->
                        updateState(NetworkState.error(error.localizedMessage))
                        setRetry(Action { loadAfter(params, callback) })
                    }
                )
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
        // not used
    }

    private fun updateState(state: NetworkState) {
        this.networkState.postValue(state)
    }

    fun retry() {
        retryCompletable?.let {
            compositeDisposable.add(retryCompletable!!.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe()
            )
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }
}
