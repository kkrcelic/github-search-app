package com.kkrelic.github.data

@Suppress("DataClassPrivateConstructor")
data class NetworkState private constructor(
    val status: ResourceStatus,
    val errorMessage: String? = null
) {
    companion object {
        val LOADING =
            NetworkState(ResourceStatus.LOADING)
        val SUCCESS =
            NetworkState(ResourceStatus.SUCCESS)
        fun error(errorMessage: String?) = NetworkState(
            ResourceStatus.ERROR,
            errorMessage
        )
    }
}
