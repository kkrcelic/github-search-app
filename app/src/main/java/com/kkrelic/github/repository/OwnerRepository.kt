package com.kkrelic.github.repository

import androidx.annotation.MainThread
import com.kkrelic.github.api.ApiService
import com.kkrelic.github.data.Owner
import io.reactivex.Observable

class OwnerRepository(private val service: ApiService) {

    @MainThread
    fun getOwnerDetails(ownerName: String): Observable<Owner> {
        return service.getOwnerDetails(ownerName)
    }
}
