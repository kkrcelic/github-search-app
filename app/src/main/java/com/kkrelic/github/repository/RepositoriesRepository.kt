package com.kkrelic.github.repository

import androidx.annotation.MainThread
import androidx.lifecycle.Transformations.switchMap
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.kkrelic.github.api.ApiService
import com.kkrelic.github.data.Listing
import com.kkrelic.github.data.RepositoriesDataSourceFactory
import com.kkrelic.github.data.Repository
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class RepositoriesRepository(private val service: ApiService) {

    @MainThread
    fun getRepositoryDetails(ownerName: String, repoName: String): Observable<Repository> {
        return service.getRepositoryDetails(ownerName, repoName)
    }

    @MainThread
    fun searchRepositories(searchQuery: String, sortType: String?): Listing<Repository> {

        val factory = repositoriesDataSourceFactory(searchQuery, sortType)

        val config = pagedListConfig()

        val livePagedList = LivePagedListBuilder(factory, config)
            .build()

        return Listing(
            pagedList = livePagedList,
            networkState = switchMap(factory.repositoriesDataSourceLiveData) { it.networkState },
            retry = { factory.repositoriesDataSourceLiveData.value?.retry() })
    }

    private fun repositoriesDataSourceFactory(searchQuery: String, sortType: String?): RepositoriesDataSourceFactory {
        return RepositoriesDataSourceFactory(
            searchQuery,
            sortType,
            service,
            CompositeDisposable()
        )
    }

    private fun pagedListConfig(): PagedList.Config {
        return PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(20)
            .setInitialLoadSizeHint(20 * 2)
            .build()
    }
}
